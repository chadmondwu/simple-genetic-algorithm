# Simple Genetic Algorithm

Uses fitness-proportionate selection to determine the generation with optimal fitness. 
Performance: often produces stale populations around a local minimum, need to implement elitism or tournament selection