/*
 Genetic Algorithm  - Chadmond Wu
 Tests a simple fitness proportionate genetic algorithm against its fitness performance through crossover and mutation
 All functions and definitions within main.c
 Saves output to run.txt file
 Known bugs: performance does not always reach max fitness

 September 2020

*/

//Header Files
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

//Macros
#define POPULATION_SIZE		100
#define GENOME_LENGTH		10
#define MAX_FITNESS         GENOME_LENGTH
#define GENE_TYPE           2               //Gene type of 0, 1
#define CROSSOVER_RATE		0.1
#define MUTATION_RATE		5.0
#define GENERATION_TOTAL	10

//Global Variables
int **currentPopulation, **nextPopulation;
int *idealGenome, *randGenome;
int *genomeFitnesses;
double *genomeProbabilities;
int totalFitness;

// Function declarations
void memoryAllocation(int size, int length);    //Keeps track of arrays

void aRandomGenome(int length);
void makePop(int size, int length);
int evalFitness(int *genome);
double evalAvgFitness(int **population);
int evalBestFitness(int **population);
void crossover(int *genome1, int *genome2);
void mutate(int *genome, double mutRate);
int selectOnePair(int **population);
int runGenetic(int populationSize, int crossoverRate, int mutationRate);

int main()
{ 
    srand(GetTickCount());  //Seed random number generator rand(), more accurate than time()
							//Dependent on windows.h, alternative is srand(time(NULL))
    int finalPopulation = 0;
    memoryAllocation(POPULATION_SIZE, GENOME_LENGTH);
    makePop(POPULATION_SIZE, GENOME_LENGTH);

    finalPopulation = runGenetic(POPULATION_SIZE, CROSSOVER_RATE, MUTATION_RATE);
    printf("The final generation was: %d\n", finalPopulation);
}

/*Helper Functions*/
//Handles dynamic memory allocation through malloc() - avoids stack/heap errors
void memoryAllocation(int size, int length)
{
    currentPopulation = (int**)malloc(size * sizeof(int*));
    nextPopulation = (int**)malloc(size * sizeof(int*));
    idealGenome = (int*)malloc(length * sizeof(int));
    randGenome = (int*)malloc(length * sizeof(int));
    genomeProbabilities = (double*)malloc(size * sizeof(double));

    for (int i = 0; i < POPULATION_SIZE; ++i)
    {
        currentPopulation[i] = (int*)malloc(length * sizeof(int));
        nextPopulation[i] = (int*)malloc(length * sizeof(int));
    }
}

//Assigns random genome of int array[length]
void aRandomGenome(int length)
{
    for (int i = 0; i < length; ++i)
    {
        randGenome[i] = rand()%GENE_TYPE;
        idealGenome[i] = rand()%GENE_TYPE;
    }
}

//Assigns population arrays of randomly generated Genomes[length], total size based on POP_SIZE
void makePop(int size, int length)
{
    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < length; ++j)
        {
            currentPopulation[i][j] = rand()%GENE_TYPE;
            nextPopulation[i][j] = rand()%GENE_TYPE;
        }
    }
}

//Evaluates fitness value (# of 1's) of a specific genome
int evalFitness(int *genome)
{
	int genomeFitness = 0;
	for (int i = 0; i < GENOME_LENGTH; i++)
	{
		if (genome[i] == 1)
			++genomeFitness;
	}
	return genomeFitness;
}

//Returns the average fitness of a given population
double evalAvgFitness(int **population)
{
    int currentFitness = 0;
    int currentTotal = 0;
    double avgFitness = 0.0;
    for (int i = 0; i < POPULATION_SIZE; ++i)
    {
        currentFitness = 0;
        for (int j = 0; j < GENOME_LENGTH; ++j)
        {
            if (currentPopulation[i][j] == 1)
            {
                ++currentFitness; 
            }
            currentTotal += currentFitness;
        }
    }
    avgFitness = (double) currentTotal / (POPULATION_SIZE*GENOME_LENGTH);
    return avgFitness;
}

//Returns the best fitness of a given population
int evalBestFitness(int **population)
{
    int currentFitness = 0; 
    int bestFitness = 0;
    for (int i = 0; i < POPULATION_SIZE; ++i)
    {
        currentFitness = 0;
        for (int j = 0; j < GENOME_LENGTH; ++j)
        {
            if (currentPopulation[i][j] == 1)
            {
                ++currentFitness; 
            }
            if (currentFitness > bestFitness)
            {
                bestFitness = currentFitness;
            }
        }
        totalFitness += currentFitness;
    }
    return bestFitness;
}

//Modifies and creates two new genomes by crossing both genomes over random crosspoint
void crossover(int *genome1, int *genome2)
{
	int tempGenome[GENOME_LENGTH] = {0};
	int crossPoint = rand()%GENOME_LENGTH + 1; //Crosspoint chosen randomly along genome
	for (int i = crossPoint; i < GENOME_LENGTH; ++i)
	{
		tempGenome[i] = genome1[i];
		genome1[i] = genome2[i];
		genome2[i] = tempGenome[i];
	}
}
//Creates new mutated genome, bit flip mutation, rate based on MUT_RATE
void mutate(int *genome, double mutRate)
{
	double randomNumber = (double)rand() / (double)((unsigned)RAND_MAX + 1);
	int mutPoint = (int)(randomNumber * GENOME_LENGTH + 1);

	if (randomNumber < mutRate)
	{
		if (genome[mutPoint] == 0)
		{
			genome[mutPoint] = 1;
		}
		else genome[mutPoint] = 0;
	}
}

//Returns a genome index from a given population using roulette wheel (fitness proportionate) selection
//Function called twice in runGenetic function to produce a 'pair'
//Selection explanation and pseudocode found within Wikipedia page for fitness proportionate selection (Sep. 2020)
int selectOnePair(int **population)
{
    double offset = 0.0;
	double randomNumber = (double)rand() / (double)((unsigned)RAND_MAX + 1); //Random number between 0 and 1
    int currentTotal, index = 0;

	for (int i = 0; i < POPULATION_SIZE; i++) 
	{
		currentTotal += evalFitness(population[i]);
	}

    for (int i = 0; i < POPULATION_SIZE; i++) 
	{
		genomeProbabilities[i] = (double)evalFitness(population[i])/currentTotal;  //Roulette wheel probability      
	}

    for (int i = 0; i < POPULATION_SIZE; ++i)
    {
        offset +=genomeProbabilities[i];
        if (randomNumber <= offset)
        {
            index = i;
            break;
        }
    }
    index = rand() % 99 + 1;
    return index;
}

//Main GA function
//Initial Population -> Fitness Evaluation -> Selection -> Crossover -> Mutation -> Fill new Population
//Returns the last generation/population, whether it reaches MAX_FITNESS or generation limit
int runGenetic(int populationSize, int crossoverRate, int mutationRate)
{
    FILE *f = fopen("run1.txt", "a");
    
    int generationTotal = 0;
    int currentBest = 0;
	double randomNumber = (double)rand() / (double)((unsigned)RAND_MAX + 1);
    printf("Population size: %d\n", POPULATION_SIZE);
    printf("Genome length: %d\n", GENOME_LENGTH);
    
    while(generationTotal < GENERATION_TOTAL)
    {
        printf("Generation %d: Average fitness %f, best fitness %d \n",generationTotal, evalAvgFitness(currentPopulation), evalBestFitness(currentPopulation));
        if(f)   //Write to file
        {
            fprintf(f, "%d %f %d \n",generationTotal, evalAvgFitness(currentPopulation), evalBestFitness(currentPopulation));
        } 
        else
        {
            return -1; //File open error
        }
        
        if (evalBestFitness(currentPopulation) > currentBest)
        {
            currentBest = evalBestFitness(currentPopulation);
        }
        if (evalBestFitness(currentPopulation) == MAX_FITNESS) //Fitness evaluation for all 1's
        {
            break;
        }
        for (int i = 0; i < POPULATION_SIZE; ++i)
        {
            int pair1 = selectOnePair(nextPopulation);  //Selection
            int pair2 = selectOnePair(nextPopulation);

            for (int j = 0; j < GENOME_LENGTH; ++j)
            {
                if (randomNumber > crossoverRate)
                {
                    crossover(currentPopulation[pair1], nextPopulation[i]); //Crossover
                    crossover(currentPopulation[pair2], nextPopulation[i]);
                }

                    mutate(currentPopulation[pair1], mutationRate); //Mutation
                    mutate(currentPopulation[pair2], mutationRate);
            }
        }
        ++generationTotal;

        for (int i = 0; i < POPULATION_SIZE; ++i)
        {
            for (int j = 0; j < GENOME_LENGTH; ++j)
            {
                currentPopulation[i][j] = nextPopulation[i][j];    //Filling new pop
            }
        }
    }
    printf("Results saved in file run.txt \n");
    fclose(f);
    printf("Best fitness: %d \n", currentBest);
    return generationTotal;
}